# clojure-memo
## Table of Contents





* __[Getting Started](#getting-started)__
* __[Lein REPL](#lein-repl)__
* __[Debugging](#debugging)__
* __[JS interop](#js-interop)__

## Getting Started

* [Clojure Doc](https://clojuredocs.org/)
* [Style Guide](https://github.com/bbatsov/clojure-style-guide)
* [Leiningen](https://github.com/technomancy/leiningen)

## Lein REPL

* Starting the REPL

  ```sh
  lein repl
  ```

* Import

  ```Clojure
  (require '[your.namespace])
  ```

* Reload

  ```Clojure
  (require 'your.namespace :reload-all)
  ```
## Debugging

* Print log

  ```Clojure
  (enable-console-print!)
  (print "foo" "bar")

  ;print with color
  (print "\033[41m" "foo" "bar" "\033[0m")
  (print "\033[42m" "foo" "bar" "\033[0m")
  (print "\033[43m" "foo" "bar" "\033[0m")
  ```

## JS interop

* Convert JS to Clojure

  ```Clojure
  (js->clj js-object)
  ```

* Convert Clojure to JS

  ```Clojure
  (clj->js clj-object)
  ```

* Create an object with `new`

  ```Clojure
  ; var foo = new Foo("Bar")
  (def foo (new js/Foo "Bar"))
  ```

* Invoke method

  ```Clojure
  ; window.alert("Foo")
  (.alert js/window "Foo") 
  ```

* Access property

  ```Clojure
  ; foo.bar
  (.-bar foo) 

  ; foo.prop1.prop2
  (.. foo -prop1 -prop2)

  (-> foo .-prop1 .-prop2)
  ```
